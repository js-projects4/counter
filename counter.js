
const buttons = document.querySelectorAll('.buttonSpace'); //importante llamar a la clase que comparten todos los botones que quiero llamar y añadir el . antes
let counter = 0; //no puede ser const porque no podria modificar su valor al sumar/restar
const numberChange = document.getElementById("number"); //meto en la constante numberChange el id que aparece en el html
const baseDivId = document.getElementById("baseDivId");

const handleClick = () => {
    buttons.forEach((button) => { //llamo button a cada boton, y hago que recorra el arreglo (const button) que es un arreglo porque son varios botones
        button.addEventListener("click", (content) => {handleCounter(content.target.id)} );
    });
}

const handleCounter = (order) => {
    if (order === "subtractButton") {
        counter = counter - 1; //otra forma para restar seria directamente poner counter--
    } else if (order === "addButton") {
        counter++; //otra formas seria: counter = counter + 1;
    } else {
        counter = 0;
    };
    numberChange.innerHTML = counter;
    handleChangeColors();
}

const handleChangeColors = () => {
    if (counter > 0) { 
        numberChange.style.color = "green";
        baseDivId.style.backgroundColor = "#e9edc9"
    } else if (counter < 0) {
        numberChange.style.color = "red";
        baseDivId.style.backgroundColor = "#fcb9b2"
    } else {
        numberChange.style.color = "black";
        baseDivId.style.backgroundColor = "#edf2f4"
    }
}

handleClick(); //llamo a la función sino no se ejecuta nada 
